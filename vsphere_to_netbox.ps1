#Create connection strings to vsphere and netbox, other parameters as well
Import-Module VMware.VimAutomation.Core
$creds = Get-VICredentialStoreItem -file  C:\Users\TestVPN\netbox\In.creds
Connect-VIServer -Server $creds.host -User $creds.User -Password $creds.Password

#Get array of Virtual machines from vmware
$Vms = Get-VM
$api_base_url = "http://192.168.0.32/api"
#Set Netbox API Headers
$headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
$headers.Add("Authorization", 'Token 3eac7d2c2918c23a2e74367970')
$headers.Add("Content-Type", 'application/json')
$headers.Add("Accept", 'application/json')
#Using foreach check every virtual machines if exists in netbox
Foreach($Vm in $Vms){
   $Vmname = $Vm.Name
   $getDeviceInfo = (Invoke-RestMethod -Uri $api_base_url/virtualization/virtual-machines/?name="$VMNAme" -Headers $headers).results
   $NetboxVMName = $getDeviceInfo.name
   Start-sleep -Milliseconds 100
   Write-Host "
    Host in Vmware $Vm
    Host in Netbox $NetboxVMName

   "
   # If virtual machine does not exist prepare Virtual machines parameters, then using netbox API add virtual machine to netbox via POST request in powershell
   if ([string]::IsNullOrEmpty($NetboxVMName)){
        $VM
        $VMname = $VM.Name
        $VMcpu = $VM.NumCpu
        $VMmemory = $VM.MemoryMB
        $VMdisk = $VM.ProvisionedSpaceGB
        $VMdisk = [math]::Round($VMdisk)
        $VMdescription = $VM.Notes
        $VMStatus = $VM.PowerState
        $VMcreated = $VM.CreateDate
        if ($VMStatus -eq "PoweredOn"){
            $VMstate = 'active'
        }
        else{
            $VMstate = 'offline'
        }
     #   $Absentinnetbox.Add($Vm)
        #Getting cluster name from vwware
        $ClusterVM = Get-Cluster -VM $Vm
        $VMinCluster = $ClusterVM.Name 
        $result = (Invoke-RestMethod -Uri $api_base_url/virtualization/clusters/?name="$VMinCluster" -Headers $headers).results
        $Clusterid = $result.id
        #Preparing data for netbox
        $VirtualMachine = @{
            name=$VMname
            vcpus=$VMcpu
            memory=$VMmemory
            disk=$VMdisk
            cluster=$Clusterid
            comments=$VMdescription
            status=$VMstate
            created=$VMcreated
        }
        $VirtualMachineJson = $VirtualMachine | ConvertTo-Json
        $Response = Invoke-RestMethod -Uri $api_base_url/virtualization/virtual-machines/ -ContentType "application/json; charset=utf-8" -Method Post -Headers $headers -Body $VirtualMachineJson
        ConvertTo-JSON $Response | Write-Verbose
        echo "True"
        Start-sleep -Milliseconds 50
    }
}
 